const messages = require('phg-messages')
const Wrapper = require('./baseplate')

const instance = new Wrapper(messages)
const baseplate = instance.baseplate
const initialize = instance.initialize

module.exports = {
    baseplate,
    initialize,
}