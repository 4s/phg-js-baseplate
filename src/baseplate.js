"use strict";

/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The JavaScript implementation of the PHG core baseplate.
 *
 * The JS baseplate is always a _slave_ baseplate, which means that it
 * does not provide a reflector or a MasterModule, and therefore
 * cannot be used alone, but must always be connected to a _master
 * baseplate_ offering these features. This connection is provided
 * through the _piping system_.
 *
 * In order to use the baseplate, instantiate the Wrapper object below
 * along with the Google Protocol Buffer (a.k.a. protobuf) generated
 * message library. Then invoke the Wrapper.initialize function with a
 * reference to the Protobuf library (the s4.messages.* namespace) and
 * the piping callback used to forward data from this JS slave
 * baseplate to the master baseplate. The initialize function will
 * then return a callback to be used when forwarding data _to_ the JS
 * baseplate from the master baseplate, and make the baseplate object
 * available for general use.
 *
 * The baseplate object contains the following 3 well-known baseplate
 * classes and one function:
 *   - ApplicationState [enum class]
 *   - ModuleBase
 *   - isAvailable()
 *
 * The isAvailable() function returns false until the initialize()
 * function has been successfully invoked on the Wrapper. The
 * ModuleBase class will not be available until this is done.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

// FIXME: A lot of errors are thrown throughout this code. When a
// strategy on exception management is in place, search through the
// entire code for "throw" statements in order to check if they still
// make sense.


/**
 * A Wrapper object contains the real PHG core JS baseplate object
 * along with a function to initialize that baseplate, connecting the
 * pipes to the master baseplate.
 * 
 * The instantiator of the Wrapper is supposed to first invoke the
 * initialize function to connect the pipes, and then expose the
 * baseplate object as a globally accessible object to allow modules
 * access to it.
 *
 * **NOTE: TBD:** Automatically constructing Core modules in the
 * Javascript world has not yet been considered. There are basically
 * three possible "types" of modules: 
 *  1. Pure Core modules - only communicating through the baseplate
 *     with no JS API.
 *  2. Mixed Core modules - both communicating through the baseplate
 *     AND expose a JS API (e.g. the Device and Measurement Engine).
 *  3. Non-core modules - a.k.a. "ordinary" JS modules
 * Obviously, (3) should NOT be constructed automatically as this
 * would be uncommon (thus, confusing) to the average JS
 * developer. And since a JS developer should not find any observable
 * difference between (2) and (3), the mixed core modules should
 * probably not be handled automatically either. That leaves only the
 * pure core modules (1).  And we may not want to have any of those
 * (because we can probably argue that any pure core module ought to
 * be implemented in C++ instead...), so we may argue that we are
 * never going to need this feature. We need to discuss the subject
 * and come to a formal decision, though.
 */
function Wrapper(messagesRoot) {

    // Validate the type of mr:
    // Must locate s4.messages.interfaces
    // and s4.messages.classes.error.Error(data)
    if (!messagesRoot || !messagesRoot.interfaces || !messagesRoot.classes || !messagesRoot.classes.error || !messagesRoot.classes.error.Error ||
        Object.prototype.toString.call(messagesRoot.classes.error.Error) !== '[object Function]' ||
        messagesRoot.classes.error.Error.length !== 1) {
        throw "Illegal message root"
    }

    /**
     * The baseplate availability. Is set to true when the connection
     * to the master baseplate has been established using
     * Wrapper.initialize().
     */
    var available = false;

    /* ************************************************** */
    /*                                                    */
    /*     Pipes connecting to the master baseplate       */
    /*                                                    */
    /* ************************************************** */

    /*
     * Connection to the master baseplate comprises two "pipes" - one
     * in each direction. A pipe is a function implemented at the
     * receiver end, which accepts a single argument of type
     * ArrayBuffer and returns void.
     *
     * The pipes are exchanged and connected when the
     * Wrapper.initialize() function is invoked.
     */


    // The fragments of the message currently being received from the
    // master baseplate
    var incomingFragments = [];

    /**
     * The pipein connects the data stream FROM the master baseplate
     * to this baseplate.
     * @param buffer An ArrayBuffer supplying the next stream fragment.
     *               (This buffer will be copied by value)
     * @returns void
     */
    function pipein(buffer) {

        // Convert buffer to a regular array of the payload and add it
        // to the incomingFragments
        var buf = Array.from(new Uint8Array(buffer));

        // The first byte in the incoming buffer is the header. In the
        // direction FROM master baseplate to slaves, this header byte
        // can assume one of the following values:
        //  0: The payload is a message fragment which is the last
        //     fragment of a message
        //     (In ZeroMQ terms: zmq_msg_more() == 0)
        //  1: The payload is a message fragment which is part of a
        //     multi-fragment message, and more fragments will follow.
        //     (In ZeroMQ terms: zmq_msg_more() != 0)
        var hasMore = buf.shift();

        incomingFragments.push(buf);

        if (hasMore === 0) {
            // This was the last fragment of a message. Publish it to
            // any subscribers

            var fragments = incomingFragments;

            // Reset the incomingFragments (We do this before the
            // publishForSubscribers call to accomodate testing this
            // baseplate implementation without a master baseplate, in
            // which case tail calls become important)
            incomingFragments = [];

            publishForSubscribers(fragments);
        }
    }

    /**
     * The real pipeout function, if defined. (Is defined by
     * Wrapper.initialize())
     */
    var _pipeout = undefined;

    /**
     * The pipeout connects the data stream TO the master baseplate
     * from this baseplate.
     * @param buffer An ArrayBuffer supplying the next stream fragment.
     * @returns void
     */
    function pipeout(buffer) {
        if (_pipeout) _pipeout(buffer);
    }



    /* ************************************************** */
    /*                                                    */
    /*        Lower-layer "ZeroMQ-like" interface         */
    /*                                                    */
    /* ************************************************** */

    /*
     * The lower layer interface exposes a PUB and a SUB socket
     * resembling the ZeroMQ interfaces used by the other
     * baseplates. The SUB socket communicates from this JS baseplate
     * to the other baseplates (it subscribes to messages from JS
     * modules, and JS publishes to this socket). The PUB socket
     * communicates from modules on other baseplates to this JS
     * baseplate (it publishes to the JS modules, so JS modules
     * subscribes to its publications).
     */

    // The next index assigned to sub socket multipart messages
    var subChannelCounter = 0;
    // Cache of multipart messages
    var subMpMessageCache = {};

    /**
     * The SUB socket
     */
    var sub = {

        /**
         * Publishing a new message (possibly a multipart message
         * comprising multiple frames). For multipart messages,
         * hasMore must be set to true, and the function will return
         * "channel" which must be used to send the following message
         * frames using publishMore.
         *
         * @param message An array of bytes (integers in the interval
         *                0-255)
         * @param hasMore True if this is the first frame in a
         *                multipart message. False if this is a
         *                singlepart message.
         * @returns null if hasMore is false, and a channel id to be
         *          used in publishMore if hasMore is true.
         */
        publishNew: function (message, hasMore) {
            if (hasMore) {
                // Multipart message
                // These messages are cached in the subMpMessageCache
                // and not sent until all messages are collected

                var channel = subChannelCounter;

                subChannelCounter++;

                // Save a copy of the message!
                subMpMessageCache[channel] = [message.slice()];

                return channel;
            } else {
                // Singlepart message
                // This message can be sent immediately

                // Cloning the buffer before modification
                var m = message.slice();

                // hasMore=false (0) is inserted as the first byte
                m.unshift(0);

                // Convert message to ArrayBuffer and send it
                var buf = (new Uint8Array(m)).buffer;
                pipeout(buf);

                return null;
            }
        },

        /**
         * Publish a frame of a multipart message.
         *
         * @param channel The channel id provided by publishNew() of a
         *                channel that has not yet been closed. If
         *                this argument is invalid or points to a
         *                closed channel, the message is silently
         *                dropped!
         * @param message An array of bytes (integers in the interval
         *                0-255)
         * @param hasMore True if more frames will follow (the channel
         *                is left open), and false if this was the
         *                last frame (the channel is closed). 
         * @returns void
         */
        publishMore: function (channel, message, hasMore) {
            if (hasMore) {
                // More fragments to come

                var msgSequence = subMpMessageCache[channel];

                if (msgSequence) {
                    // Save a copy of the message!
                    msgSequence.push(message.slice());
                }

            } else {
                // Last fragment
                // We iterate through all fragments to send them

                var msgSequence = subMpMessageCache[channel];

                if (msgSequence) {

                    // Iterate through all the previously cached fragments...
                    for (var i in msgSequence) {

                        var m = msgSequence[i];

                        // hasMore=true (1) is inserted as the first byte
                        m.unshift(1);

                        // Convert m to ArrayBuffer and send it
                        var buf = (new Uint8Array(m)).buffer;
                        pipeout(buf);
                    }

                    // ...then, finally send the last message

                    // Cloning the buffer before modification
                    var m = message.slice();

                    // hasMore=false (0) is inserted as the first byte
                    m.unshift(0);

                    // Convert message to ArrayBuffer and send it
                    var buf = (new Uint8Array(m)).buffer;
                    pipeout(buf);

                    // Delete the cached messages
                    delete subMpMessageCache[channel];
                }
            }
        }
    };

    /*
     * Hashmap of subscriptions from modules on this baseplate using
     * the callback.toString() as key.  Each element is an array of
     * objects of the form: { callback: f, prefixes:
     * ['prefix1','prefix2'] } where f is the callback function.
     */
    var pubModuleSubscriptions = {};

    /*
     * Tree of subscriptions registrered at the master baseplate via
     * the pipe. Each node is an object containing a {subscribers: []}
     * list of subscribing callbacks as well as mapping from keys
     * 0-255 to a similar node.
     */
    var pubMasterSubscriptions = { subscribers: [] };

    /*
     * Add a subscriber registered with this topic in
     * pubMasterSubscriptions
     * @param topic   An array of bytes (integers in the interval 0-255)
     * @param node    The node in the pubMasterSubscriptions tree topic
     *                is relative to
     * @param callback A function accepting a single argument
     *                 which is an array of arrays of
     *                 bytes. I.e. an array of message fragment
     *                 comprising a single (single- or multipart)
     *                 message.
     * @returns true if the increase for this topic went from 0->1
     */
    function addSubscriber(topic, node, callback) {
        if (topic.length) {
            // We are not at the proper node yet, recursively step
            // ahead

            var head = topic.shift();
            if (!node[head]) {
                // Create node if it doesn't exist already
                node[head] = { subscribers: [] };
            }

            // Invoke recursively
            return addSubscriber(topic, node[head], callback);

        } else {
            // We are at the proper node. Add the callback and return
            // true if the count was increased to 1.

            node.subscribers.push(callback);
            return (node.subscribers.length === 1);
        }
    }

    /*
     * Find subscribers registered with any prefix of this topic in
     * pubMasterSubscriptions
     * @param topic   An array of bytes (integers in the interval 0-255)
     * @param node    The node in the pubMasterSubscriptions tree prefix
     *                is relative to
     * @returns an array of callback functions registered to this topic
     */
    function lookupSubscribers(topic, node) {

        // We build the list of subscribers one at a time here
        var subscribers = [];

        if (topic.length) {
            // We walk the tree as deep as possible, collecting subscribers

            var head = topic.shift();
            if (node[head]) {
                subscribers = lookupSubscribers(topic, node[head]);
            }
        }

        // Add subscribers at this level to the list one at a time,
        // checking for duplicates
        // NOTE/FIXME: This is an inefficient algorithm. If
        // performance is important this may have to change!

        for (var s in node.subscribers) {
            if (subscribers.indexOf(node.subscribers[s]) < 0) {
                subscribers.push(node.subscribers[s]);
            }
        }

        return subscribers;
    }

    /*
     * Remove a subscriber registered with this topic in
     * pubMasterSubscriptions
     * @param topic   An array of bytes (integers in the interval 0-255)
     * @param node    The node in the pubMasterSubscriptions tree topic
     *                is relative to
     * @param callback A function accepting a single argument
     *                 which is an array of arrays of
     *                 bytes. I.e. an array of message fragment
     *                 comprising a single (single- or multipart)
     *                 message.
     * @returns true if the decrease for this topic went from 1->0
     */
    function rmSubscriber(topic, node, callback) {
        if (topic.length) {
            // We are not at the proper node yet, recursively step
            // ahead

            var head = topic.shift();
            if (!node[head]) {
                // Node does not exist. Return false as there was no
                // current subscription to cancel
                return false;
            }

            // Invoke recursively
            return rmSubscriber(topic, node[head], callback);

        } else {
            // We are at the proper node. Remove the callback and
            // return true if the count was decreased to 0.

            var idx = node.subscribers.indexOf(callback);
            if (idx < 0) return false;
            node.subscribers.splice(idx, 1);

            return node.subscribers.length === 0;
        }
    }

    /**
     * Publish a message to all subscribers of the topic of that
     * message.
     * @param fragments An array of arrays of bytes representing a
     *                  single ZeroMQ message possibly comprising
     *                  multiple fragments.
     */
    function publishForSubscribers(fragments) {
        // The topic is always the beginning of the first fragment
        var topic = fragments[0].slice();

        // Produce a list of subscribers matching this message
        var subscribers = lookupSubscribers(topic,
            pubMasterSubscriptions);


        // Send the message to all subscribers
        for (var s in subscribers) {
            // Create a deep copy of the message in order to
            // ensure complete separation between subscribers!
            var msg = [];
            for (var f in fragments) {
                msg.push(fragments[f].slice());
            }

            // publish to subscriber
            subscribers[s](msg);
        }
    }




    /**
     * The PUB socket
     */
    var pub = {

        /**
         * Register the given callback function as a subscriber to the
         * given message prefix. See the ZeroMQ manual for details on
         * how subscription to prefixes work. A callback may subscribe
         * to multiple prefixes. Details are here:
         * https://rfc.zeromq.org/spec:29/PUBSUB/ and 
         * https://rfc.zeromq.org/spec:23/ZMTP/#the-publish-subscribe-pattern
         *
         * @param prefix  An array of bytes (integers in the interval
         *                0-255)
         * @param callback A function accepting a single argument
         *                 which is an array of arrays of
         *                 bytes. I.e. an array of message fragment
         *                 comprising a single (single- or multipart)
         *                 message.
         */
        subscribePrefix: function (prefix, callback) {

            // The key into the pubModuleSubscriptions hashmap
            var key = callback.toString();

            // Extract or add the array of subscription elements
            var elementArray = pubModuleSubscriptions[key];
            if (!elementArray) {
                elementArray = [];
                pubModuleSubscriptions[key] = elementArray;
            }

            // Extract or add the subscription element
            var element = null;
            for (var e in elementArray) {
                if (elementArray[e].callback === callback) {
                    element = elementArray[e];
                    break;
                }
            }
            if (!element) {
                element = { callback: callback, prefixes: [] };
                elementArray.push(element);
            }

            // Add the subscription prefix (copying the array!)
            element.prefixes.push(prefix.slice());

            // Increase the number of subscribers to this topic
            if (addSubscriber(prefix.slice(), pubMasterSubscriptions,
                callback)) {
                // If the increase was 0->1, also send a subscribe to
                // the master baseplate

                // Cloning the buffer before modification
                var m = prefix.slice();

                // subscribe (2) is inserted as the first byte
                m.unshift(2);

                // Convert message to ArrayBuffer and send it
                var buf = (new Uint8Array(m)).buffer;
                pipeout(buf);
            }
        },

        /**
         * Unregister the given callback function as a subscriber to
         * the given message prefix.
         *
         * @param prefix  An array of bytes (integers in the interval
         *                0-255)
         * @param callback The function used in the corresponding
         *                 subscribePrefix call (must be a === match).
         */
        unsubscribePrefix: function (prefix, callback) {

            // The key into the pubModuleSubscriptions hashmap
            var key = callback.toString();

            // Extract the array of subscription elements
            var elementArray = pubModuleSubscriptions[key];
            if (!elementArray) return;

            // Extract the subscription element
            var element = null;
            var elementIdx = -1;
            for (var e in elementArray) {
                if (elementArray[e].callback === callback) {
                    element = elementArray[e];
                    elementIdx = e;
                    break;
                }
            }
            if (!element) return;

            // Locate the subscription prefix index in prefixes
            var idx = element.prefixes.findIndex(function (candidate) {
                return prefix.length === candidate.length
                    && prefix.every(function (value, index) {
                        return value === candidate[index]
                    });
            });
            if (idx < 0) return;

            // Remove prefix
            element.prefixes.splice(idx, 1);

            // Decrease the number of subscribers to this topic
            if (rmSubscriber(prefix.slice(), pubMasterSubscriptions,
                callback)) {
                // If the decrease was 1->0, also send an unsubscribe
                // to the master baseplate

                // Cloning the buffer before modification
                var m = prefix.slice();

                // unsubscribe (3) is inserted as the first byte
                m.unshift(3);

                // Convert message to ArrayBuffer and send it
                var buf = (new Uint8Array(m)).buffer;
                pipeout(buf);
            }

            // Tidy up: Remove element from elementArray if no more
            // subscriptions, otherwise return
            if (element.prefixes.length) return;
            elementArray.splice(elementIdx, 1);

            // Tidy up: Remove elementArray from
            // pubModuleSubscriptions if empty
            if (elementArray.length) return;
            delete pubModuleSubscriptions[key];
        }
    };



    /* ************************************************** */
    /*                                                    */
    /*                Utility functions                   */
    /*                                                    */
    /* ************************************************** */

    /*
     * A collection of utility functions used in the higher layer
     * implementation found below
     */

    /**
     * Convert an ASCII string to a byte array.
     * @param str A string which is only allowed to contain character
     *            codes in the interval 0-127
     * @return A plain array of the str characters
     */
    function stringToBytes(str) {
        if (str === null) return [];
        var bytes = [];
        for (var i = 0; i < str.length; ++i) {
            var code = str.charCodeAt(i);
            if (code <= 127) {
                bytes.push(code);
            }
            else {
                // ignore non-ascii characters
                console.log("Illegal character " + code);
            }
        }
        return bytes;
    }

    /**
     * Translate a PascalCase or camelCase string to underscore_case
     * @param s The string to convert
     * @returns The converted string
     */
    function camelToUnderscore(s) {
        return s.charAt(0).toLowerCase()
            + s.substring(1).replace(/([A-Z]*)([A-Z][a-z])/g, "$1_$2")
                .toLowerCase();
    }

    /**
     * Generate new "unique" string - FOR DEMO PURPOSES ONLY!!!
     * FIXME! 
     */
    var uniqueCounter = 10009753; // Most random number in the world...
    function unique() {
        uniqueCounter += 1;
        return uniqueCounter.toString();
    }

    /*
     * Generate a new unique Peer ID
     *
     * FIXME: No guarantee that this is unique!!!
     */
    function createPeer() {
        return unique();
    }

    /*
     * Generate a new unique ObjId ID
     *
     * FIXME: No guarantee that this is unique!!!
     */
    function createObjId() {
        return unique();
    }

    /*
     * Generate a module-level unique callback ID
     */
    var callbackCounter = 0;
    function createCallbackId() {
        callbackCounter += 1;
        return callbackCounter;
    }



    /* ************************************************** */
    /*                                                    */
    /*  Higher-layer baseplate-exported types and classes */
    /*                                                    */
    /* ************************************************** */

    /*
     * The classes and enum types directly exported by the baseplate:
     *   - ApplicationState [enum class]
     *   - CoreError
     *   - ModuleBase
     *
     * Classes indirectly exported by the baseplate (returned by some of
     * the ModuleBase functions):
     *   - InterfaceEndpoint
     *   - MetaData
     *   
     * and the Header class and AddressingMode enum, which are used
     * internally to model first message fragment.
     */


    /**
     * ApplicationState enum
     * 
     * Represents the global application state of the PHG core which
     * is maintained and announced by the MasterModule on the master
     * baseplate.
     *
     * Used by the ModuleBase.getApplicationState() and
     * ModuleBase.onApplicationStateChange() functions.
     */
    var ApplicationState = {
        0: "INITIALIZING", "INITIALIZING": 0,
        1: "READY", "READY": 1,
        2: "RUNNING", "RUNNING": 2,
        3: "SHUTDOWN_REQUEST", "SHUTDOWN_REQUEST": 3,
        4: "FINALIZING", "FINALIZING": 4
    }

    /**
     * AddressingMode enum
     * 
     * Represents the destination type of a message.
     * 
     * Used in the Header class
     */
    var AddressingMode = {
        85: "UNICAST", 77: "MULTICAST", 66: "BROADCAST",
        "UNICAST": 85, "MULTICAST": 77, "BROADCAST": 66
    }

    /**
     * MetaData class
     *
     * This class represents the metadata received along with a message.
     */
    function MetaData(header) {
        this.unicasted = (header.mode == AddressingMode.UNICAST); // bool
        this.sender = header.sender;                              // Peer
        this.more = header.isMore();                              // bool
    }

    /**
     * CoreError class
     *
     * Represents a run-time error / exception.
     * Create the minimal CoreError object with only the two
     * mandatory elements.
     */
    function CoreError(moduleTag, message) {

        /**
         * The name of the interface.
         *
         * If a protobuf package name is
         * “s4.messages.interfaces.foo_bar”, this interface_name must
         * be “foo_bar”
         */
        this.interfaceName = undefined;

        /**
         * Machine readable error code.
         *
         * A number scoped by interface_name, and defined in the
         * interface docs.
         */
        this.errorCode = undefined;

        /**
         * The mandatory name of the module in which this error
         * message was recorded. Also known as the TAG. A module named
         * "Foo bar-baz" should use the PascalCase equivalent
         * "FooBarBaz" as moduleTag.
         */
        this.moduleTag = moduleTag;

        /**
         * A mandatory short error message (at most a few lines)
         */
        this.message = message;

        /**
         * An optional longer (multi-line) detailed description of the
         * error (e.g. a stack trace or dump)
         */
        this.details = undefined;

        /**
         * The optional name (and perhaps full path) of the source
         * file where the error was detected
         */
        this.fileName = undefined;

        /**
         * The line number in the source file where the error was
         * detected (only allowed together with file).
         */
        this.lineNumber = undefined;

        /**
         * The optional name (perhaps fully qualified) of the class where
         * the error was detected
         */
        this.className = undefined;

        /**
         * The optional name of the function/method/subprocedure where the
         * error was detected
         */
        this.functionName = undefined;

        /**
         * An optional nested CoreError that was the root cause of this
         * error.
         */
        this.causedBy = undefined;
    }



    // FIXME: The Header class is not yet fully aligned with the canonical design

    /**
     * Header class
     * 
     * The static parse method parses a byte buffer into a new Header
     * object. The serialize method returns a serialized byte buffer.
     */
    function Header(mode, receiver, subject, signal, sender, callbackDeclId) {
        this.mode = mode;                     // AddressingMode
        this.receiver = receiver;             // Peer
        this.subject = subject;               // string
        this.signal = signal;                 // string
        this.sender = sender;                 // Peer
        this.callbackDeclId = callbackDeclId; // int
    }

    /**
     * Parses a header string and creates a Header object
     * @param buf An array of bytes (integers in the interval 0-255)
     */
    Header.parse = function (buf) {
        var receiver = "";
        var subject = "";
        var signal = "";
        var sender = "";
        var callbackDeclId = 0;
        var mode = buf.shift();
        var headerString = String.fromCharCode.apply(String, buf);
        var parts = headerString.split('_');
        // FIXME: Overvej om der er grund til at splitte i 3 cases her?
        if (mode == AddressingMode.UNICAST && parts.length >= 5) {
            // unicast
            receiver = parts[0];
            subject = parts[1];
            signal = parts[2];
            sender = parts[3];
            if (parts.length >= 6) {
                callbackDeclId = parseInt(parts[4], 16);
            }
        }
        else if (mode == AddressingMode.MULTICAST &&
            parts.length >= 5) {
            // multicast
            subject = parts[1];
            signal = parts[2];
            sender = parts[3];
            if (parts.length >= 6) {
                callbackDeclId = parseInt(parts[4], 16);
            }
        }
        else if (mode == AddressingMode.BROADCAST &&
            parts.length == 2) {
            // FIXME: Skal testes og dobbelttjekkes. Det lyder ikke rigtigt med length==2 og har broadcastbeskeder ikke afsender?
            // broadcast
            subject = parts[1];
            signal = parts[2];
        }
        else {
            throw "Invalid " + AddressingMode[mode] + " header: " +
            headerString;
        }
        return new Header(mode, receiver, subject, signal, sender,
            callbackDeclId);
    };

    Header.prototype.isUnicast = function () {
        return this.mode == AddressingMode.UNICAST;
    };

    Header.prototype.isMulticast = function () {
        return this.mode == AddressingMode.MULTICAST;
    };

    Header.prototype.isBroadcast = function () {
        return this.mode == AddressingMode.BROADCAST;
    };

    Header.prototype.isMore = function () {

    };

    /**
     * Returns a string representation of this header.
     */
    Header.prototype.toString = function () {
        var header = "";
        header += String.fromCharCode(this.mode);
        if (this.mode == AddressingMode.UNICAST) {
            header += this.receiver;
        }
        header += "_";
        if (typeof (this.subject) !== 'string') return header;
        header += this.subject;
        header += "_";
        if (typeof (this.signal) !== 'string') return header;
        header += this.signal;
        header += "_";
        if (typeof (this.sender) !== 'string' || this.sender == "")
            return header;
        header += this.sender;
        header += "_";
        if (typeof (this.callback) !== 'number' || this.callback == 0)
            return header;
        header += ("0000" + this.callbackDeclId.toString(16)).slice(-4);
        return header;
    };

    /**
     * Serialize this header into a byte array
     */
    Header.prototype.serialize = function () {
        return stringToBytes(this.toString());
    }




    /**
     * The abstract parent class of any PHG core module
     *
     * The ModuleBase class implements the common elements of all
     * modules, and each module (or module-like component) shall
     * inherit from (or keep one instance of) this class.
     *
     * **Note**
     * This ModuleBase is lacking a few features compared to the C++
     * and Java couterparts; most notably, shutdown handling and
     * logging is not included yet. Please use application-specific
     * logging instead for now...
     * 
     * @param moduleTag The mandatory name of this module used when a
     *                  log or error message is recorded. Also known
     *                  as the TAG. A module named "Foo bar-baz module"
     *                  should use the PascalCase equivalent
     *                  "FooBarBaz" as module_tag
     */

    function ModuleBase(moduleTag) {

        // Initialize stuff...

        // My peer identity
        this._id = createPeer();
        this._moduleTag = moduleTag;

        // Subscribe to all broadcasts from the MasterModule
        pub.subscribePrefix((new Header(AddressingMode.BROADCAST)).serialize(),
            function (msg) {
                // Handle incoming broadcast messages from the
                // MasterModule

                console.log("FIXME: Unhandled Broadcast message received");
                console.log(msg);
            });

    }

    /**
     * Start the module.
     *
     * Calling this function is allowed only once, typically as
     * the final act of constructing the most derived object. This
     * signifies that all interfaces of this module have been
     * declared and the module may be connected to the PHG core
     * system.
     */
    ModuleBase.prototype.start = function () {

    }

    /**
     * Get the Peer ID of this module.
     *
     * This will return the id used to address this module in
     * UNICAST messages. This is an opaque id that is generated by
     * the baseplate and assigned to this module during the
     * construction.
     *
     * @return The Peer ID of this module
     */
    ModuleBase.prototype.getId = function () {
        return this._id;
    }

    /**
     * Get the moduleTag id of this module.
     *
     * This will return the TAG used to identify log messages from
     * this module. Any CoreError objects constructed in this module
     * should use this moduleTag.
     *
     * @return The moduleTag set for this module
     */
    ModuleBase.prototype.getModuleTag = function () {
        return this._moduleTag;
    };

    /**
     * Get the current ApplicationState of the PHG core system.
     *
     * @return The current ApplicationState
     */
    ModuleBase.prototype.getApplicationState = function () {
        // FIXME: Not implemented
        return ApplicationState.RUNNING;
    }

    /**
     * Declare that this module implements the producer end of an
     * interface.
     *
     * Calling this function is only allowed after the constructor
     * but before calling start(). Typically this only happens
     * inside the constructor of the derived objects.
     *
     * @return The InterfaceEndpoint to be used for communication
     *         on this interface.
     */
    ModuleBase.prototype.implementsProducerInterface = function (interfaceName) {
        return new InterfaceEndpoint(this, interfaceName, "C2P", "P2C");
    }

    /**
     * Declare that this module implements the consumer end of an
     * interface.
     *
     * Calling this function is only allowed after the constructor
     * but before calling start(). Typically this only happens
     * inside the constructor of the derived objects.
     *
     * @return The InterfaceEndpoint to be used for communication
     *         on this interface.
     */
    ModuleBase.prototype.implementsConsumerInterface = function (interfaceName) {
        return new InterfaceEndpoint(this, interfaceName, "P2C", "C2P");
    }

    /**
     * Report that the PHG core application state has changed.
     *
     * Derived classes may override this function to handle
     * ApplicationState changes.
     */
    ModuleBase.prototype.onApplicationStateChange
        = function (newApplicationState) {
            // Dummy implementation - does nothing. Can be overridden
            // by module implementations
        }

    /**
     * Generate and return a fresh ObjId object id.
     *
     * In order to share objects across module boundaries, each
     * object must be uniquely identified by an object ID. The
     * type for this ID is defined as an opaque type by the
     * baseplate: ObjId. When an ID is generated in this way,
     * the baseplate guarantees a system-wide unique
     * identification of the object.
     *
     * @return A fresh ObjId
     */
    ModuleBase.prototype.createObjId = createObjId;

    /*
     * The InterfaceEndpoint class represents a single interface
     * produced or consumed by a module.
     */

    function InterfaceEndpoint(module, interfaceName, incoming, outgoing) {
        this.interfaceProto = messagesRoot.interfaces[camelToUnderscore(interfaceName)];
        this.inInterfaceProto = messagesRoot.interfaces[camelToUnderscore(interfaceName)][incoming];
        this.outInterfaceProto = messagesRoot.interfaces[camelToUnderscore(interfaceName)][outgoing];
        this.interfaceName = interfaceName;
        this.inSubject = interfaceName + "." + incoming;
        this.outSubject = interfaceName + "." + outgoing;

        this.module = module;

        this.functionMap = new Map();
        this.eventMap = new Map();

        var endpoint = this;

        // Handler of the incoming messages on this interface
        function handler(msg) {

            // All messages on this interface must have exactly two
            // fragments - the header and payload

            if (msg.length != 2) {
                throw new Error("Received message did not comprise exactly two fragments");
            }

            var header = Header.parse(msg[0]);

            // Tests / asserts for subscription consistency - should never fail!

            if (header.mode === AddressingMode.UNICAST) {
                if (header.receiver !== module.getId()) {
                    throw new Error("Received message intended for another module");
                }
            } else if (header.mode != AddressingMode.MULTICAST) {
                throw new Error("Received message that was neither a multicast or unicast");
            }
            if (header.subject !== endpoint.inSubject) {
                throw new Error("Received message for a different interface");
            }

            var meta = new MetaData(header);


            // Parse the arguments
            var payload = endpoint.inInterfaceProto.deserializeBinary(msg[1]);

            // Discover the message contents
            if (payload.hasEvent && payload.hasEvent()) {

                // We got an event
                var event = payload.getEvent();

                // We shouldn't get a callback
                if (header.callbackDeclId && header.callbackDeclId != 0) {
                    // FIXME
                    // We warn on the console
                    console.warn("Callback received for an Event");
                }

                // Iterate through registered event handlers
                var found = false;
                for (var [name, implementation] of endpoint.eventMap) {
                    if (event["has" + name]()) {

                        // We found the implementation. Now invoke it.
                        found = true;
                        implementation(event["get" + name](), meta);
                        break;
                    }
                }
                if (!found) {
                    throw new Error("An event was received but not handled on the "
                        + endpoint.interfaceName + " interface");
                }

            } else if (payload.hasRequest && payload.hasRequest()) {

                // We got a function invocation
                var func = payload.getRequest();

                // We must also have a callback
                if (!(header.callbackDeclId && header.callbackDeclId != 0)) {
                    // FIXME
                    throw "No callback received for a Function request";
                }

                // Build the serialized callback header
                var callbackHeader = (new Header(AddressingMode.UNICAST, header.sender,
                    "#" + ("0000" + header.callbackDeclId.toString(16)).slice(-4),
                    "", module.getId()));


                // Iterate through registered function handlers
                var found = false;
                for (var [name, implementation] of endpoint.functionMap) {
                    if (func["has" + name]()) {

                        // We found the implementation.
                        found = true;

                        // Check the existence of the return type
                        if (!endpoint.interfaceProto[name + "Success"]) {
                            throw new Error("Could not find a definition of the response type for request "
                                + name);
                        }

                        // Define a callback to handle responses
                        var callback = function (retVal, hasMore) {

                            // Set the hasMore flag in the callback header

                            if (hasMore) {
                                callbackHeader.signal = "KEEP";
                            } else {
                                callbackHeader.signal = "LAST";
                            }

                            if (retVal instanceof CoreError) {
                                // Returning an error
                                callbackHeader.signal = "ERR-" + callbackHeader.signal;

                                // Change type of retVal from CoreError
                                // to s4.messages.classes.Error

                                var protoError = new messagesRoot.classes.error.Error();

                                // Move retVal (CoreError) content to protoError
                                protoError.setInterfaceName(retVal.interfaceName);
                                protoError.setErrorCode(retVal.errorCode);
                                protoError.setModuleTag(retVal.moduleTag);
                                protoError.setMessage(retVal.message);
                                protoError.setDetails(retVal.details);
                                protoError.setFileName(retVal.fileName);
                                protoError.setClassName(retVal.className);
                                protoError.setFunctionName(retVal.functionName);
                                protoError.setLineNumber(retVal.lineNumber);
                                protoError.setCausedBy(retVal.causedBy);

                                // and redefine retVal
                                retVal = protoError;
                            }


                            // Publish header and payload to the ZeroMQ socket

                            var channel = sub.publishNew(callbackHeader.serialize(), true);
                            sub.publishMore(channel, Array.from(retVal.serializeBinary()), false);
                        }

                        // Now invoke the implementation
                        implementation(func["get" + name](),
                            endpoint.interfaceProto[name + "Success"],
                            callback, messagesRoot, meta);
                        break;
                    }
                }
                if (!found) {
                    throw new Error("A function call was received but not handled on the "
                        + endpoint.interfaceName + " interface");
                }

            } else {
                // should not be possible
                throw new Error("Received an unknown message type that could not be handled");
            }
        }
        // Subscribe to all multicasts for this interface
        pub.subscribePrefix((new Header(AddressingMode.MULTICAST, null,
            this.inSubject)).serialize(), handler);
        // Subscribe to all unicasts for this interface
        pub.subscribePrefix((new Header(AddressingMode.UNICAST, module.getId(),
            this.inSubject)).serialize(), handler);
    }


    /**
     * Call a function handled by the remote end of this interface.
     *
     * @param functionName The name of the function as a string using
     *                     PascalCase (mandatory)
     * @param argumentProvider A function taking a Protobuf object
     *                         containing the arguments of this function
     *                         and setting each argument as needed by
     *                         the function.
     * @param destination The implementer of this function
     * @param successCallback Callback to be invoked (perhaps
     *                        multiple times) when the function
     *                        returns normally. The
     *                        successCallback and errorCallback
     *                        functions may be called many times
     *                        with the 'more' flag set to true. On
     *                        the final callback, the 'more' flag
     *                        will be false.
     * @param errorCallback Callback to be invoked (perhaps
     *                      multiple times) if an error is
     *                      returned from the function.
     */
    InterfaceEndpoint.prototype.callFunction
        = function (functionName, argumentProvider, destination, successCallback, errorCallback) {
            if (this.outInterfaceProto.Request === undefined) {
                throw new Error("This version of the " + this.outSubject +
                    " interface does not provide any functions");
            }

            if ((new this.outInterfaceProto.Request())["has" + functionName] ===
                undefined || this.interfaceProto[functionName] === undefined) {
                throw new Error("This version of the " + this.outSubject +
                    " interface does not provide a " + functionName +
                    " function");
            }

            if (destination === undefined) {
                throw new Error("The destination is mandatory");
            }

            // Fetch arguments from the argumentProvider

            var outputArguments = new this.interfaceProto[functionName]();
            if (argumentProvider !== undefined) {
                argumentProvider(outputArguments, messagesRoot);
            }

            // Build the payload of the function request

            var request = new this.outInterfaceProto.Request();
            request["set" + functionName](outputArguments);
            var payload = new this.outInterfaceProto();
            payload.setRequest(request);

            // Define the callback and handler

            var callbackId = createCallbackId();
            var module = this.module;
            var successProto = this.interfaceProto[functionName + "Success"];

            function handler(msg) {

                // All messages on this interface must have exactly two
                // fragments - the header and payload

                if (msg.length != 2) {
                    throw new Error("Received message did not comprise exactly two fragments");
                }

                var header = Header.parse(msg[0]);

                // Tests / asserts for subscription consistency - should never fail!

                if (header.mode !== AddressingMode.UNICAST ||
                    header.receiver !== module.getId()) {
                    throw new Error("Received message of wrong type or receiver");
                }
                if (header.subject !== "#" + ("0000" + callbackId.toString(16)).slice(-4)) {
                    throw new Error("Received message for a different interface");
                }
                // We shouldn't get a callback
                if (header.callback && header.callback != "") {
                    // FIXME
                    // We warn on the console
                    console.warn("Callback received for a callback");
                }

                if (header.signal.substr(0, 4) == "ERR-") {
                    // Parse the error
                    var payload = messagesRoot.classes.error.Error
                        .deserializeBinary(msg[1]);

                    var err = new CoreError(payload.getModuleTag(), payload.getMessage());

                    err.interfaceName = payload.getInterfaceName();
                    err.errorCode = payload.getErrorCode();
                    err.details = payload.getDetails();
                    err.fileName = payload.getFileName();
                    err.className = payload.getClassName();
                    err.functionName = payload.getFunctionName();
                    err.lineNumber = payload.getLineNumber();
                    err.causedBy = payload.getCausedBy();


                    // This is an error callback
                    errorCallback(err, header.signal.substr(4) == "KEEP");
                } else {
                    // This is a success callback

                    // Parse the arguments
                    var payload = successProto.deserializeBinary(msg[1]);

                    successCallback(payload, header.signal == "KEEP");
                }
                //HERTIL
                // FIXME : unsubscribe hvis LAST

            }

            // and subscribe to the callbacks
            pub.subscribePrefix((new Header(AddressingMode.UNICAST, this.module.getId(),
                "#" + ("0000" + callbackId.toString(16)).slice(-4))).serialize(), handler);

            // Build the header of the function request

            var header;
            header = new Header(AddressingMode.UNICAST, destination,
                this.outSubject,
                "",
                this.module.getId(), callbackId);

            // Publish header and payload to the ZeroMQ socket

            var channel = sub.publishNew(header.serialize(), true);
            sub.publishMore(channel, Array.from(payload.serializeBinary()), false);
        }

    /**
     * Post an event on this interface.
     * 
     * @param eventName The name of the event as a string using
     *                  PascalCase (mandatory)
     * @param argumentProvider A function taking a Protobuf object
     *                         containing the arguments of this event
     *                         and setting each argument as needed by
     *                         the event.
     * @param destination The optional recipient of this event (if not
     *                    defined, the event will be multicasted)
     */
    InterfaceEndpoint.prototype.postEvent
        = function (eventName, argumentProvider, destination) {
            // Locate the event's interface definition

            if (this.outInterfaceProto.Event === undefined) {
                throw new Error("This version of the " + this.outSubject +
                    " interface does not provide any events");
            }

            if ((new this.outInterfaceProto.Event())["has" + eventName] ===
                undefined || this.interfaceProto[eventName] === undefined) {
                throw new Error("This version of the " + this.outSubject +
                    " interface does not provide a " + eventName +
                    " event");
            }

            // Fetch arguments from the argumentProvider

            var outputArguments = new this.interfaceProto[eventName]();
            if (argumentProvider !== undefined) {
                argumentProvider(outputArguments, messagesRoot);
            }

            // Build the payload of the event

            var event = new this.outInterfaceProto.Event();
            event["set" + eventName](outputArguments);
            var payload = new this.outInterfaceProto();
            payload.setEvent(event);

            // Build the header of the event

            var header;
            if (destination === undefined) { // Multicast
                header = new Header(AddressingMode.MULTICAST, null,
                    this.outSubject,
                    "",
                    this.module.getId());
            } else { // Unicast
                header = new Header(AddressingMode.UNICAST, destination,
                    this.outSubject,
                    "",
                    this.module.getId());
            }

            // Publish header and payload to the ZeroMQ socket

            var channel = sub.publishNew(header.serialize(), true);
            sub.publishMore(channel, Array.from(payload.serializeBinary()), false);
        }


    /**
     * Register the handler of a function implemented at this end of
     * the interface.
     * 
     * The implementation argument is a void function taking the
     * following arguments:
     *   - args: The Protobuf object containing the arguments
     *   - outputClass: The Protobuf *class* to be instantiated for
     *     the return payload
     *   - callback: A void function taking two arguments: argsOrError,
     *     which must be either an object of the outputClass *or* an
     *     CoreError error message object; and more, which is a boolean
     *     indicating whether more responses will come later.
     *   - messagesRoot: The Protobuf messageRoot namespace
     *     (s4.messages) to be used for constructing content to the
     *     return payload.
     *   - meta: The MetaData of the function call
     *
     * @param functionName The name of the function as a string using
     *                     PascalCase (mandatory).
     * @param implementation The implementation of this function.
     *                       args and meta contains the input to
     *                       this function. The function may respond
     *                       multiple times using the callback (the
     *                       last time with 'more' set to false). A
     *                       response must have an argsOrError either
     *                       an outputClass instance or an CoreError
     *                       instance.
     */
    InterfaceEndpoint.prototype.addFunctionHandler
        = function (functionName, implementation) {
            if (this.functionMap.has(functionName)) {
                throw new Error("Attempting to add handler for function " +
                    functionName + " twice");
            }
            if (this.inInterfaceProto.Request &&
                (new this.inInterfaceProto.Request())["has" + functionName]) {
                this.functionMap.set(functionName, implementation);
            } else {
                throw new Error("Attempt to register function handler for unknown function "
                    + functionName);
            }
        }


    /**
     * Register the handler of an event implemented at this end of
     * the interface.
     * 
     * The implementation argument is a void function taking the
     * following arguments:
     *   - args: The Protobuf object containing the arguments
     *   - meta: The MetaData of the function call
     *
     * @param eventName The name of the event as a string using
     *                  PascalCase (mandatory)
     * @param implementation The implementation of this function.
     *                       args and meta contains the input to
     *                       this function.
     */
    InterfaceEndpoint.prototype.addEventHandler
        = function (eventName, implementation) {
            if (this.eventMap.has(eventName)) {
                throw new Error("Attempting to add handler for event " +
                    eventName + " twice");
            }
            if (this.inInterfaceProto.Event &&
                (new this.inInterfaceProto.Event())["has" + eventName]) {
                this.eventMap.set(eventName, implementation);
            } else {
                throw new Error("Attempt to register event handler for unknown event "
                    + eventName);
            }
        }

    /**
     * Create an CoreError object intended for a function handler error
     * callback.
     *
     * The produced CoreError object will have interfaceName, errorCode,
     * moduleTag, and message prepared, based on this
     * InterfaceEndpoint and the createError function's arguments.
     *
     * @param errorCode The error code of the error
     * @param message The message of the error
     * @return A new CoreError object
     */
    InterfaceEndpoint.prototype.createError = function (errorCode, message) {
        var err = new CoreError(this.module.getModuleTag(), message);
        err.interfaceName = camelToUnderscore(this.interfaceName);
        err.errorCode = errorCode;
        return err;
    }



    /* ************************************************** */
    /*                                                    */
    /*             Baseplate initialization               */
    /*                                                    */
    /* ************************************************** */

    /**
     * Initialize and connect this baseplase to the rest of the PHG
     * core system.
     * @param out The stream in the direction out of this baseplate
     * @returns The stream in the direction into this baseplate or null if any
     *          argument was deemed non-acceptable
     */
    this.initialize = function (out) {
        // Validate the type of out (function of one argument)
        if (!out || Object.prototype.toString.call(out) !== '[object Function]'
            || out.length !== 1) {
            return null; // The argument was not acceptable
        }

        _pipeout = out;

        // Expose the ModuleBase constructor
        this.baseplate.ModuleBase = ModuleBase;

        // This baseplate is now available.
        available = true;

        return pipein;
    }


    /**
     * The Baseplate object
     */
    this.baseplate = {

        /**
         * The Baseplate (ModuleBase class) is unavailable until
         * properly initialized. The availability property is set to
         * true when the connection to the master baseplate has been
         * established using Wrapper.initialize()
         *
         * The application should check baseplate availability before
         * ever attempting to use the baseplate.
         */
        isAvailable: function () { return available; },

        /**
         * ApplicationState enum
         * 
         * Represents the global application state of the PHG core which
         * is maintained and announced by the MasterModule on the master
         * baseplate.
         */
        ApplicationState: ApplicationState,

        /**
         * The error message class
         *
         * Represents a run-time error / exception.
         */
        CoreError: CoreError,

        /**
         * The abstract parent class of any PHG core module
         *
         * The ModuleBase class implements the common elements of all
         * modules, and each module (or module-like component) shall
         * inherit from (or keep one instance of) this class.
         *
         * The ModuleBase is unavailable until Wrapper.initialize()
         * has been successfully invoked.
         */
        ModuleBase: undefined
    }
}
module.exports = Wrapper
