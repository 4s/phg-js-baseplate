
JavaScript PHG Core Baseplate Tutorials
=======================================

This folder contains tutorials intended as an introduction to the use
of the JavaScript baseplate. A prerequisite to understanding these
tutorials would be the general PHG core and baseplate overview.
__FIXME:__ *link*

The tutorials are organised as pairs of HTML and JavaScript (or
TypeScript) files. The HTML file contains nothing but a simple user
interface, while the script file contains the actual tutorial,
including a lot of explanations. So open the tutorial script file in a
text editor and the HTML file in a web browser and have fun!


Preparing for the Tutorials
---------------------------

The tutorials need the compiled Protocol Buffer messages to be
available in a file called `messages.js`, which must be built using
the `protoc`compiler.

__FIXME:__ *Building the `messages.js`file*



Tutorial 1: Calculating Primes
------------------------------

__FIXME:__ *not yet implemented for JavaScript*



Tutorial 2: Serial port communication
-------------------------------------

The goal of this tutorial is to demonstrate how a complex interface
can be used between multiple modules. The SerialPort interface is used
as an example of this, and the following points are demonstrated:

  -  Multiple modules: 1 Producer (producing two SerialPorts) and 2
     Consumers (each of which is capable of opening both SerialPorts)
  -  Functions with multiple callbacks: Reading from a SerialPort is
     done using repeated use of the Open function callback.
  -  Multicast and unicast events: Used for signalling
  -  Interface errors: Competing for the same resource may lead to an
     error when two consumers access the same SerialPort.
     Furthermore, writing to a port that has not been opened by the
     Consumer performing the write will lead to an error.
 
 This tutorial can be found in the `tutorial_02.js`file, and the user
 interface for this tutorial can be found in the `tutorial_02.html`
 file. Open this HTML file in a browser to test the tutorial.
