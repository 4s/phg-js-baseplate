"use strict";

/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Tutorial 02: Demonstrating the complex SerialPort interface
 *
 * The goal of this tutorial is to demonstrate how a complex interface
 * can be used between multiple modules. The SerialPort interface is
 * used as an example of this, and the following points are
 * demonstrated:
 *
 *  -  Multiple modules: 1 Producer (producing two SerialPorts) and 2
 *     Consumers (each of which is capable of opening both
 *     SerialPorts)
 *  -  Functions with multiple callbacks: Reading from a SerialPort is
 *     done using repeated use of the Open function callback.
 *  -  Multicast and unicast events: Used for signalling
 *  -  Interface errors: Competing for the same resource may lead to an
 *     error when two consumers access the same SerialPort.
 *     Furthermore, writing to a port that has not been opened by the
 *     Consumer performing the write will lead to an error.
 *
 * The user interface for this tutorial can be found in the
 * tutorial_02.html file. Open this HTML file in a browser to test the
 * tutorial
 * 
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */


/*
 * The following two definitions depends on how the external code was
 * linked to this tutorial. Your mileage may vary...
 */

// Define the imported baseplate module
var wrapper = module.exports;
// and compiled protobuf messages
var messageRoot = proto.s4.messages;




/* ******************************************************* */
/*                                                         */
/*          Piping and a trivial reflector-mock            */
/*                                                         */
/* ******************************************************* */

/*
 * The following section implements a trivial reflector at the piping
 * level in order to demonstrate the functionality of this JS
 * baseplate in an isolated environment (without a full master
 * baseplate).
 *
 * For debugging and demonstration purposes, this reflector will print
 * all reflected messages to the JavaScript console.
 *
 * NOTE THAT THE BEHAVIOUR OF THE BASEPLATE IS DIFFERENT WITH THIS
 * REFLECTOR: A "normal" reflector will serialize messages, ensuring
 * that a message is not handled until the previous handler
 * returns. This reflector, on the other hand, will handle messages
 * recursively (so the handler of the message being sent will be
 * recursively executed before the sender can finish). Therefore, we
 * have to make extensive use of tail calls throughout the tutorial
 * code. This is, however, not required in ordinary modules executing
 * on a real baseplate.
 *
 */



// Primitive reflector:
var pipe = undefined;
pipe = wrapper.initialize(messageRoot, function(buf) {
    var array = new Uint8Array(buf);
    console.log("Piping received: " +
                JSON.stringify(String.fromCharCode.apply(String, array)));
    if (array[0] > 1) {
        // filter out subscribe/unsubscribe messages
        return;
    }
    // Reflect
    pipe && pipe(buf);
});


// At this point the baseplate should be successfully initialized and
// ready.
// If not, something must be wront with the wrapper or
// messageRoot defined at the top of this script.

var baseplate = wrapper.baseplate;
console.info("baseplate.isAvailable(): " + baseplate.isAvailable());




/* ******************************************************* */
/*                                                         */
/*          String to/from Uint8Array conversion           */
/*                                                         */
/* ******************************************************* */


function bytesToString(bytes) {
    var result = "";
    for (var i = 0; i < bytes.length; i++) {
        if (bytes[i] < 16) {
            result += "%0" + bytes[i].toString(16);
        } else if (bytes[i] < 32 || bytes[i] == 37 || bytes[i] > 126) {
            result += "%" + bytes[i].toString(16);
        } else {
            result += String.fromCharCode(bytes[i]);
        }
    }
    return result;
}

function stringToBytes(str) {
    var bytes = new Uint8Array(str.length);
    for (var i = 0; i < str.length; ++i) {
        var code = str.charCodeAt(i);
        if (code <= 255) {
            bytes[i] = code;
        }
        else {
            // ignore non-ascii characters
            console.log("Illegal character!");
        }
    }
    return bytes;
}




/* ******************************************************* */
/*                                                         */
/*                    The two modules                      */
/*                                                         */
/* ******************************************************* */

/*
 * This tutorial creates three modules, a SerialPort producer and two
 * SerialPort consumers.
 * 
 * The producer exposes two ports (the id of which is handed to the
 * consumer on a "side channel"). A consumer will open a port send
 * some data, wait for a response, and close the port.
 */

// The application shall not attempt to create and launch (or access)
// any module if the baseplate is not available.

if (baseplate.isAvailable()) {



    /*
     *   The Producer
     */
    

    // Step 1: Any module extends the abstract ModuleBase class
    
    // Create a module object extending ModuleBase (on the global
    // Window object in order to be reachable from the GUI)
    
    window.producerModule = new baseplate.ModuleBase('PRODUCER');


    
    // Step 2: Business functionality exist in the subclass
    
    // The producer module "produces" real serial ports as represented
    // by the following class. In this tutorial, this "serial port" is
    // implemented by an HTML GUI component.

    function RealSerialPort(index) {
        // The index is used to link this instance of the object to
        // its GUI counterpart
        this.index = index;
        
        // The serial port, we expose, has the following identity
        this.handle = producerModule.createObjId();

        // The state of the serial port - true when open
        this.isOpen = false;

        // Receive function. Will be replaced by a real implementation
        // below.
        this.onReceived = null;
        
        // Port closed event. Will be replaced by a real implementation
        // below.
        this.onClosed = null;
    }
    
    // Open the port 
    RealSerialPort.prototype.open = function() {
        if (this.isOpen) {
            // Already open! Throw exception
            console.error('PRODUCER: Attempt to open an open port ' +
                          this.index);
            throw "Already open";
        } else {
            console.info('PRODUCER: The serial port ' + this.index +
                         ' was opened');
            this.isOpen = true;
            document.getElementById("pIsOpen" + this.index).innerHTML
                = "Open";
        }
    };
    
    // Close the port
    RealSerialPort.prototype.close = function() {
        if (this.isOpen) {
            console.info('PRODUCER: The serial port ' + this.index
                         + ' was closed');
            this.isOpen = false;
            document.getElementById("pIsOpen" + this.index).innerHTML
                = "Closed";
            // Publish the event
            this.onClosed && this.onClosed();
        } else {
            console.warn('PRODUCER: Attempt to close a closed port ' +
                         this.index);
        }
    };
    
    // Writing to the serial port will display the message on the
    // GUI
    RealSerialPort.prototype.write = function(message) {
        if (!this.isOpen) {
            // Not open! Throw exception
            console.error('PRODUCER: Attempt to write to a closed port '
                          + this.index);
            throw "Not open";
        } else {
            console.info('PRODUCER: Writing data for port ' + this.index);
            var e = document.createElement("li");
            e.innerHTML = bytesToString(message);
            document.getElementById("pReceived" + this.index).appendChild(e);
        }
    };
    

    // We create two instances of the RealSerialPort and store the
    // (handler,handle) pairs in an array on the Window object, from
    // where the consumers may "discover" them

    producerModule.port0 = new RealSerialPort(0);
    producerModule.port1 = new RealSerialPort(1);

    // Information about active connections to consumers are stored
    // here
    producerModule.port0connection =
        {outputClass: null, callback: null, peer: null};
    producerModule.port1connection =
        {outputClass: null, callback: null, peer: null};

    console.info("PRODUCER: Serial port 0 created with objId: "
                 + producerModule.port0.handle);
    console.info("PRODUCER: Serial port 1 created with objId: "
                 + producerModule.port0.handle);

    
    // This is how the consumers will learn the identity of the
    // ports. In a real application this information will be exchanged
    // as a service discovery on a separate interface.
    window.producedPorts =
        [{
            handler: producerModule.getId(),
            handle:  producerModule.port0.handle
        },{
            handler: producerModule.getId(),
            handle:  producerModule.port1.handle
        }];

    // The GUI button presses will manipulate the two port objects
    
    // GUI Send button pressed
    producerModule.send = function(index,message) {
        var port = index == 0 ? producerModule.port0 : producerModule.port1;
        if (port.isOpen) {
            // Send if open
            console.info('PRODUCER: Serial port ' + index + ' produced data');
            port.onReceived && port.onReceived(stringToBytes(message));
        }
    }
    
    // GUI Close button pressed
    producerModule.close = function(index) {
        if (index == 0) {
            producerModule.port0.close();
        } else {
            producerModule.port1.close();
        }
    }
    
    


    // Step 3: The module declares and implements its interfaces

    // The producer module implements the producer-end of the
    // SerialPort interface.

    // First, declare the interface endpoint:
    
    var producerEndpoint
        = producerModule.implementsProducerInterface("SerialPort");

    // Then, "attach" the implementation of this interface to this
    // endpoint. Each function and event handler is added one by one:
    //   1. A handler for the 'Open' function
    //   2. A handler for the 'Write' function
    //   3. A handler for the 'Close' event

    // Handle an incoming Open function call
    producerEndpoint.addFunctionHandler("Open",
        function (input, outputClass, callback, messagesRoot, meta) {
            var serialPortHandle = input.getSerialPortHandle();
            console.info("SerialPort.Open received for port: " +
                         serialPortHandle);

            // Dispatch to the intended RealSerialPort
            var port = null;
            var connection = null;
            if (serialPortHandle === producerModule.port0.handle) {
                port = producerModule.port0;
                connection = producerModule.port0connection;
            } else if (serialPortHandle === producerModule.port1.handle) {
                port = producerModule.port1;
                connection = producerModule.port1connection;
            }
            if (port != null) {
                try {
                    // Open port
                    port.open();

                    // Save the outputClass, callback and sender for
                    // later callbacks
                    connection.outputClass = outputClass;
                    connection.callback = callback;
                    connection.peer = meta.sender;
            
                    // Send an empty Open callback response
                    callback(new outputClass(), true);

                } catch (err) {
                    if (err == "Already open") {                    
                        // Error 2, Port already open

                        callback(producerEndpoint.createError(2,
                                                "Port already open"), false);
                    }
                }
           } else {
               // Error 1, invalid or missing argument
               callback(producerEndpoint.createError(1,
                                            "Invalid argument"), false);
            }
        });

    // Handle an incoming Write function call
    producerEndpoint.addFunctionHandler("Write",
        function (input, outputClass, callback, messagesRoot, meta) {
            var serialPortHandle = input.getSerialPortHandle();
            console.info("SerialPort.Write received for port: " +
                         serialPortHandle);
            
            var port = null;
            var connection = null;
            if (serialPortHandle === producerModule.port0.handle) {
                port = producerModule.port0;
                connection = producerModule.port0connection;
            } else if (serialPortHandle === producerModule.port1.handle) {
                port = producerModule.port1;
                connection = producerModule.port1connection;
            }
            if (port != null) {
                try {
                    // Check that the consumer attempting the write
                    // actually owns this port
                    if (meta.sender != connection.peer) {
                        throw "Not open";
                    }

                    var message = input.getMessage();
                    
                    port.write(message);
                    
                    // Send an empty Write callback response
                    callback(new outputClass(), false);
                } catch (err) {
                    if (err == "Not open") {
                        // Error 3, Port not open
                        callback(producerEndpoint.createError(3,
                                               "Port not open"), false);
                    }
                }
            } else {
                // Error 1, invalid or missing argument
                callback(producerEndpoint.createError(1,
                                             "Invalid argument"), false);
            }
        });

    // Handle an incoming Close event
    producerEndpoint.addEventHandler("Close",
        function (input, meta) {
            var serialPortHandle = input.getSerialPortHandle();
            var port = null;
            var connection = null;
            if (serialPortHandle === producerModule.port0.handle) {
                port = producerModule.port0;
                connection = producerModule.port0connection;
            } else if (serialPortHandle === producerModule.port1.handle) {
                port = producerModule.port1;
                connection = producerModule.port1connection;
            }
            
            // Also check that the consumer attempting the close
            // actually owns this port. It will be ignored when sent
            // from a different consumer than the one who currently
            // uses the port.
            if (port != null && meta.sender == connection.peer) {
                port.close();
            }
        });

    // The serial port handler for posting "Closed" events when a port
    // is closed
    producerModule.onClosed = function(port, connection) {

        if (connection.callback) {
            // Send the last empty Open callback response
            connection.callback(new connection.outputClass(), false);
            // Clear the connection
            connection.outputClass = null;
            connection.callback = null;
            connection.peer = null;
        }
        
        // Send (Multicast) a notification to all interested consumers
        producerEndpoint.postEvent("Closed",
            function (output, messagesRoot) {
                output.setSerialPortHandle(port.handle);
            });
    }

    // The serial port handler for sending input data when a port
    // received data
    producerModule.onReceived = function(connection, message) {

        // Ignore if no active connection
        if (connection.callback && connection.outputClass) {
        
            // Generate the payload
            var args = new connection.outputClass();
            args.setMessage(message);
            // Send using the Open callback response (keep open)
            connection.callback(args, true);
        }
    
    }

    // Add the above two handlers to the RealSerialPort objects

    producerModule.port0.onClosed = function() {
        producerModule.onClosed(producerModule.port0,
                                producerModule.port0connection);
    };
    producerModule.port1.onClosed = function() {
        producerModule.onClosed(producerModule.port1,
                                producerModule.port1connection);
    };
    producerModule.port0.onReceived = function(message) {
        producerModule.onReceived(producerModule.port0connection, message);
    };
    producerModule.port1.onReceived = function(message) {
        producerModule.onReceived(producerModule.port1connection, message);
    };

    
    
    
    /*
     *   The Consumers
     */


    // Step 1: The remote serial port is represented by a local proxy
    //         defined by the following ProxySerialPort class
    

    // The state of the serial port seen from the consumer (This is
    // just a simplified state machine. In a real implementation more
    // states would be needed)
    const PORT_STATE_CLOSED = 0;
    const PORT_STATE_OPENING = 1;
    const PORT_STATE_OPEN = 2;
    const PORT_STATE_CLOSING = 3;

    // The consumer module keeps proxy objects representing the serial
    // ports. In this tutorial, these objects can be manipulated by
    // the GUI.

    function ProxySerialPort(index, handler, handle, endpoint) {
        // The index is used to link this instance of the object to
        // its GUI counterpart
        this.index = index;

        // The handler and handle pair is the key to access and
        // manipulate the remote object
        this.handler = handler;
        this.handle = handle;

        // The InterfaceEndpoint we are using for the communication
        this.endpoint = endpoint;
        
        // The state of the serial port - true when open
        this.portState = PORT_STATE_CLOSED;

        // Receive function. Will be replaced by a real implementation
        // below.
        this.onReceived = null;
        
        // Port open/closed event. Will be replaced by a real
        // implementation below.
        this.onPortStateChanged = null;

    }

    // Open the port
    ProxySerialPort.prototype.open = function() {
        if (this.portState === PORT_STATE_CLOSED) {
            this.portState = PORT_STATE_OPENING;
            this.onPortStateChanged &&
                this.onPortStateChanged();
            var psp = this;
            this.endpoint.callFunction(
                "Open",
                function (output, messagesRoot) {
                    output.setSerialPortHandle(psp.handle);
                },
                this.handler,
                function (input, more) {
                    if (psp.portState == PORT_STATE_OPENING) {
                    psp.portState = PORT_STATE_OPEN;
                        psp.onPortStateChanged &&
                            psp.onPortStateChanged();
                        console.info('CONSUMER: The serial port was opened');
                    }
                    if (input.getMessage().length != 0) {
                        psp.onReceived &&
                            psp.onReceived(input.getMessage());
                    }
                },
                function (error, more) {
                    console.error("CONSUMER: Received error: "
                                  + JSON.stringify(error));
                }
            );
        } else {
            throw "Cannot open a port when it is not closed";
        }
    };

    // Close the port
    ProxySerialPort.prototype.close = function() {
        if (this.portState === PORT_STATE_OPEN) {
            this.portState = PORT_STATE_CLOSING;
            this.onPortStateChanged &&
                this.onPortStateChanged();
            var handle = this.handle;
            // Post a *unicast* event
            this.endpoint.postEvent(
            "Close",
            function (output, messagesRoot) {
                output.setSerialPortHandle(handle);
            },
            this.handler
            );
        }
    };

    // Write a message to the port
    ProxySerialPort.prototype.write = function(message) {

        // We COULD check the state to prevent sending messages when
        // the port is not open, but that would not be any fun... We
        // like to demonstrate exceptions in this tutorial too :-)
        
        var psp = this;
        this.endpoint.callFunction(
            "Write",
            function (output, messagesRoot) {
                output.setSerialPortHandle(psp.handle);
                output.setMessage(message);
            },
            this.handler,
            function (input, more) {
                console.info('CONSUMER: Write success received');
            },
            function (error, more) {
                console.error("CONSUMER: Received error: "
                              + JSON.stringify(error));
            });            
    };

    // This is a callback to handle the 'Closed' event from the remote
    // serial port
    ProxySerialPort.prototype.closedCallback = function (input, meta) {
        if (input.getSerialPortHandle() === this.handle) {
            var portState = this.portState;
            if (portState != PORT_STATE_CLOSED) {
                this.portState = PORT_STATE_CLOSED;
                this.onPortStateChanged &&
                    this.onPortStateChanged();
                console.info('CONSUMER: The serial port was closed');
            }
            // We ignore some of the differences of being in the
            // Open, Opening or Closed states
            if (portState == PORT_STATE_OPEN ||
                portState == PORT_STATE_OPENING) {
                // Send close to confirm the state change
                var handle = this.handle;
                // Post a *unicast* event
                this.endpoint.postEvent(
                    "Close",
                    function (output, messagesRoot) {
                        output.setSerialPortHandle(handle);
                    },
                    this.handler
                );
            }
        }
    };






    // Step 2: Any module extends the abstract ModuleBase class
    
    // Create a module class extending baseplate.ModuleBase

    function ConsumerModule(tag,index) {
        // Super class constructor
        baseplate.ModuleBase.apply(this,[tag]);

        // The index is used to link this instance of the object to
        // its GUI counterpart
        this.index = index;


        

        // Step 3: The module declares and implements its interfaces
        
        // The consumer class implements the consumer-end of the
        // SerialPort interface.

        // First, declare the interface endpoint:
        
        this.consumerEndpoint =
            this.implementsConsumerInterface("SerialPort");
        
        // We maintain a local proxy for each port in the
        // global window.producedPorts structure

        this.ports = [];
        var cm = this;

        for (var i=0; i<producedPorts.length; i++) {
            var port = new ProxySerialPort(i, producedPorts[i].handler,
                                           producedPorts[i].handle,
                                           this.consumerEndpoint);
            this.ports.push(port);
            port.onPortStateChanged = (
                function(port) {
                    return function() {cm.updatePortState(port);};
                })(port);
            port.onReceived = (
                function (port) {
                    return function(msg) {cm.receivedMsg(port, msg);};
                })(port);
        }

        
        // Now, "attach" the implementation of the serial port
        // interface to the endpoint. There is only one event:        
        //   1. A handler for the 'Closed' event

        this.consumerEndpoint.addEventHandler("Closed",
            function (input, meta) {
                // The event is distributed to all ports.
                // Each port will check if it is relevant.
                for (var i=0; i<cm.ports.length; i++) {
                    cm.ports[i].closedCallback(input, meta);
                }
            });
        
    }
    // Inherit from the super class the JavaScript way...
    ConsumerModule.prototype = new baseplate.ModuleBase()



    
    // Step 4: Business functionality exist in the subclass
    
    // Set the GUI port status
    ConsumerModule.prototype.updatePortState = function(port) {
        var portState = port.portState;
        var stateName;
        if (portState == PORT_STATE_OPEN) {
            stateName = "Open";
        } else if (portState == PORT_STATE_OPENING) {
            stateName = "Opening";
        } else if (portState == PORT_STATE_CLOSING) {
            stateName = "Closing";
        } else {
            stateName = "Closed";
        }            
        document.getElementById("cState" + this.index +
                                port.index).innerHTML = stateName;
    }

    // Display a received message on the GUI
    ConsumerModule.prototype.receivedMsg = function(port, msg) {
        console.info('CONSUMER' + this.index + ': Received data on port ' +
                    port.index);

        var e = document.createElement("li");
        e.innerHTML = bytesToString(msg);
        document.getElementById("cReceived" + this.index +
                                port.index).appendChild(e);
    }
    
    
    // GUI Open button pressed
    ConsumerModule.prototype.open = function(index) {
        try {
            console.info('CONSUMER' + this.index +
                         ': Serial port ' + index + 
                         ' will be opened');
            this.ports[index].open();
        } catch (err) {
            console.error('CONSUMER' + this.index +
                          ': Could not open port ' + index +
                          ': ' + err);
        }
    }

    // GUI Close button pressed
    ConsumerModule.prototype.close = function(index) {
        try {
            console.info('CONSUMER' + this.index +
                         ': Serial port ' + index + 
                         ' will be closed');
            this.ports[index].close();
        } catch (err) {
            console.error('CONSUMER' + this.index +
                          ': Could not close port ' + index +
                          ': ' + err);
        }
    }

    // GUI Send button pressed
    ConsumerModule.prototype.send = function(index,message) {
        try {
            console.info('CONSUMER' + this.index +
                         ': Writing to port ' + index);
            this.ports[index].write(stringToBytes(message));
        } catch (err) {
            console.error('CONSUMER' + this.index +
                          ': Could not write to port ' + index +
                          ': ' + err);
        }
    }

    
    
    // Step 5: Instantiate the two consumer modules

    window.consumerModule0 = new ConsumerModule('CONSUMER0',0);
    window.consumerModule1 = new ConsumerModule('CONSUMER1',1);
    
}
