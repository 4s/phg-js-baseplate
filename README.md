
Baseplate implementation for JavaScript
=======================================
This is the baseplate implementation for modules executing in a
JavaScript interpreter.

Documentation
-------------
This is an early prototype and not much documentation is available
at this point apart from inline comments in the source code.

Dependencies
------------
This project depends on the js version of messages(https://www.npmjs.com/package/phg-messages)
The dependency is installed by running 'npm i' as is typical for
js projects.

Publishing
----------
Versions are a combination of the versions in package.json and
a git label with that version. New versions are created by
running:
```bash
npm version (major/minor/patch)
```
That command will update the package.json version and create
the git label.

Remember to push with the --follow-tags parameter subsequently.

Using
-----
This project pulls its own dependencies, so using it is simply
a matter of pulling a certain tag. An example of use would be to
add the following to a client projects package.json:
```
"dependencies": {
    "phg-js-baseplate": "git+https://bitbucket.org/4s/phg-js-baseplate.git#v0.0.11"
}
```
Typically javascript projcts will not depend directly on this
project but on projects that utilize it. An example could be
the DeviceMeasurementEngine (https://bitbucket.org/4s/device-measurement-engine)

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0