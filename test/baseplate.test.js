const { expect } = require("chai");
const wrapper = require("../src/baseplate");
const fakeMessageRoot = {};
var realMessageRoot;

describe("Baseplate basic test", () => {
  beforeEach(() => {
    // Mock window as phg messages requires it
    global.window = {};
    realMessageRoot = require("phg-messages");
  });

  it("fake message root should fail", () => {
    expect(() => new wrapper(fakeMessageRoot)).to.throw();
  });

  it("Real messages should not fail", () => {
    const bp = new wrapper(realMessageRoot);
    expect(bp).to.exist;
  });

  it("Wrapper has a baseplate that is uninitialized", () => {
    const bp = new wrapper(realMessageRoot);
    expect(bp.baseplate.isAvailable()).to.be.false;
    expect(bp.baseplate.ModuleBase).to.be.undefined;
  });

  it("Wrapper has an initialize", () => {
    const bp = new wrapper(realMessageRoot);
    expect(bp.initialize).to.exist;
  });

  it("Can be initialized", () => {
    const outPipeMock = function(buf) {};
    const bp = new wrapper(realMessageRoot);
    const pipeout = bp.initialize(outPipeMock);
    expect(pipeout).to.exist;
    expect(bp.baseplate.isAvailable()).to.be.true;
    const moduleBase = new bp.baseplate.ModuleBase();
    expect(moduleBase).to.exist;
  });

  it("can construct module bases, consumers and producers", () => {
    const outPipeMock = function(buf) {
      pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    const moduleBase = new bp.baseplate.ModuleBase("something something");

    let measurementConsumer = moduleBase.implementsConsumerInterface(
      "fhir_measurement"
    );
    measurementConsumer.addEventHandler("MeasurementReceived", () => {});

    let measurementProducer = moduleBase.implementsProducerInterface(
      "fhir_measurement"
    );
    expect(measurementConsumer).to.exist;
    expect(measurementProducer).to.exist;
  });

  it("can post event, without anything blowing up", () => {
    const outPipeMock = function(buf) {
      pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    const moduleBase = new bp.baseplate.ModuleBase("something something");

    let measurementProducer = moduleBase.implementsProducerInterface(
      "fhir_measurement"
    );
    measurementProducer.postEvent("MeasurementReceived", undefined);
  });

  it("can not call function, if no handler is added", () => {
    const outPipeMock = function(buf) {
      pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    const moduleBase = new bp.baseplate.ModuleBase("something something");

    let measurementProducer = moduleBase.implementsProducerInterface(
      "fhir_measurement"
    );
    expect(() =>
      measurementProducer.callFunction(
        "MeasurementReceived",
        undefined,
        "something",
        () => {},
        () => {}
      )
    ).to.throw;
  });

  it("can call pipeIn, without anything blowing up", () => {
    const outPipeMock = function(buf) {
      pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    pipeIn([
      [66, 95],
      [77, 95, 102, 104, 105, 114, 95, 109, 101, 97, 115, 117, 114, 101],
      [85, 49, 48, 48, 48, 57, 55, 53, 52, 95, 102, 104, 105, 114, 95],
      [77, 95, 102, 104, 105, 114, 95, 109, 101, 97, 115, 117, 114, 101],
      [18, 2, 10, 0]
    ]);
  });

  it("call outpipe when event handler is registered", () => {
    let wasCalled = false;
    const outPipeMock = function(buf) {
      (wasCalled = true), pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    const moduleBase = new bp.baseplate.ModuleBase("something something");

    let measurementConsumer = moduleBase.implementsConsumerInterface(
      "fhir_measurement"
    );
    measurementConsumer.addEventHandler("MeasurementReceived", () => {});

    expect(wasCalled).to.be.true;
  });

  it("call outpipe when event is posted", () => {
    let wasCalled = false;
    const outPipeMock = function(buf) {
      (wasCalled = true), pipeIn(buf);
    };
    const bp = new wrapper(realMessageRoot);
    const pipeIn = bp.initialize(outPipeMock);
    const moduleBase = new bp.baseplate.ModuleBase("something something");

    let measurementProducer = moduleBase.implementsProducerInterface(
      "fhir_measurement"
    );
    measurementProducer.postEvent("MeasurementReceived", undefined);

    expect(wasCalled).to.be.true;
  });
});
